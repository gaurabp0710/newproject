import {Router} from "express"


let productRouter = Router()

productRouter
.route("/")

.post((req, res)=>{
    console.log(req.body)
    res.json("I am product method post")
})

.get((req, res, next)=>{
    console.log('This is third middleware')
    res.json('terewewe')
    next()
}, (req, res, next) => {
    console.log('This is middleware second.')
    next()
})

.patch((req, res)=>{
    res.json("I am product method patch")
})

.delete ((req, res)=>{
    res.json("I am product method delete")
})

productRouter

.route("/:id/aa/:id2") //localhost:8000/products/12

.get((req, res)=>{
    console.log(req.params)
    console.log(req.query)
    res.json("I will show the detail page.")
})



export default productRouter