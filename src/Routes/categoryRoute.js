import {Router} from "express"

let categoryRouter = Router()

categoryRouter

.route("/")

.post((req, res)=>{
    res.json("I am category method post")
})

.get((req, res)=>{
    res.json("I am category method get")
})

.patch((req, res)=>{
    res.json("I am category method patch")
})

.delete ((req, res)=>{
    res.json("I am category method delete")
})

export default categoryRouter