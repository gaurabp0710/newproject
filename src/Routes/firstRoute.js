import {Router} from "express"

let firstRouter = Router()

firstRouter
.route("/admin")

.get((req, res)=>{
    res.json("I am admin method get")
})

.post((req, res)=>{
    res.json("I am admin method post")
})

.patch((req, res)=>{
    res.json("I am admin method patch")
})

.delete ((req, res)=>{
    res.json("I am admin method delete")
})

firstRouter 

.route("/admin/name")

.post((req,res)=>{
    res.json("I am admin name post")
})
.get((req,res)=>{
    res.json("I am admin name get")
})
.patch((req,res)=>{
    res.json("I am admin name patch")
})
.delete((req,res)=>{
    res.json("I am admin name delete")
})

export default firstRouter