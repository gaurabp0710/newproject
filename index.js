import express, { json } from "express";
import firstRouter from "./src/Routes/firstRoute.js"
import productRouter from "./src/Routes/productRoute.js"
import categoryRouter from "./src/Routes/categoryRoute.js"
import { HttpStatus } from "./src/config/constant.js";

let app = express();
let port = 8000

app.use(json());

app.use((req,res,next)=>{
    console.log("I am first")
    next()
})

app.use((req,res,next)=>{
    console.log("I am second")
    next()
})

app.use("/", firstRouter)
app.use("/products", productRouter)
app.use("/category", categoryRouter)

app.use((err,req,res,next)=>{
    console.log('uouoooooooooo')
    res.status(HttpStatus.OK).json('get successful')
    next('abcd')
})

app.listen(port, ()=>{
    console.log(`We are at the port, ${port}`)
})

